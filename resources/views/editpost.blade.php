
<div class="row">
	<div class="col-md-12">
	<form action="{{route('update.post')}}" method="post">
		@csrf
		<div class="form-group">
			<textarea class="form-control" name="edit_body" cols="30" rows="6">{{$data->body}}</textarea>
		</div>
		<input type="hidden" name="update_id" value="{{$data->id}}">
		<button type="submit" class="btn btn-primary float-right">Update Post</button>
	</form>
</div>
</div>