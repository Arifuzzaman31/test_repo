@if ($errors->any())
    <div class="alert alert-danger col-md-3 offset-md-4 my-4">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('message'))
	<div class="alert alert-primary text-center"><b>{{ Session::get('message') }}</b></div>
@endif