@extends('layout.master')

@section('title')
    Welcome!
@endsection

@section('content')
    @include('includes.message-block')
    <div class="row mt-2"> 
        <div class="col-md-5 border py-4">
            <h4>Sign Up</h4>
            <form action="{{route('user-sign-up.post')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="email">Your-Email</label>
                    <input type="text" class="form-control {{ $errors->has('email')? 'has-error' : '' }}" name="email" id="email" value="{{Request::old('email')}}">
                </div>
                <div class="form-group">
                    <label for="first-name">Your-Name</label>
                    <input type="text" class="form-control {{ $errors->has('name')? 'has-error' : '' }}" name="name" id="first-name" value="{{Request::old('name')}}">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control {{ $errors->has('password')? 'has-error' : '' }}" name="password" id="password" placeholder="Enter Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>   
            </form>
        </div>
        <div class="col-md-5 offset-md-2 border py-4">
            <h4>Sign in</h4>
            <form action="{{route('user-login.post')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="e-mail">Your Email</label>
                    <input type="text" class="form-control" name="email" id="e-mail">
                </div>
                <div class="form-group">
                    <label for="user-password">Password</label>
                    <input type="password" class="form-control" name="password" id="user-password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>   
            </form>
        </div>
    </div>
@endsection