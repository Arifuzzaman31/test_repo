@extends('layout.master')
@section('title')
	dashboard
@endsection
@section('content')
 @include('includes.message-block')
	<section class="row new-post">
		<div class="col-md-6 offset-md-3">
			<header><h3>What do you have to say ?</h3></header>
			<form action="{{route('create.posts')}}" method="post">
				@csrf
				<div class="form-group">
					<textarea class="form-control" name="body" id="new-post" cols="30" rows="6"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Create Post</button>
			</form>
		</div>
	</section>
	<section class="row posts"> <?php //echo "<pre>"; print_r($posts); ?>
		<div class="col-md-6 offset-md-3">
			<header><h3 class="mt-2">What Other People say ...</h3></header>
			@foreach($posts as $post)
				<article class="post">
					<p>{{$post->body}}</p>
					<div class="info">
						posted by {{$post->user->name}} on {{$post->created_at}}
					</div>
					<div class="interaction">
						<a style="text-decoration: none;" id="vote-{{$post->id}}" onclick="LikeUnlike('vote-to-post','{{$post->id}}','{{Session::get('admin_id')}}')">
							@if($post->voted)
									@if($post->voted->vote == 1) Unlike 
										@else Like 
									@endif
								@else Like
							@endif
						</a>
						@if(Session::get('admin_id') == $post->user_id)
						|
						<a style="text-decoration: none;" onclick="edit('edit.post/','{{$post->id}}')">Edit |</a>
						<a style="text-decoration: none;" href="{{ route('delete.post',$post->id )}}" onclick=" return confirm('Are You Sure ?')">Delete</a>
						@endif
					</div>
				</article>
			@endforeach
		</div>
	</section>
@endsection
@section('script')
<script type="text/javascript">
	function LikeUnlike(url,post_id,user_id){
		$.ajax({
			url: url,
			type: 'GET',
			data: {
				'post_id': post_id, 'user_id': user_id
			},
			success: function(response){
				console.log(response.status);
				if (response.status == true) {
					$("#vote-"+post_id).html('Like');
				}else{
					$("#vote-"+post_id).html('Unlike');
				}
				location.reload();
			},
			error: function(response){
				console.log(response);
			}
		});
	}

	function edit(url,id){
		// alert('hi');
		$("#modal").modal();
		$(".modal-title").text('Edit post');
		$('.modal-body').load(url+id);
	}
</script>
@endsection