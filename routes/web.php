<?php

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => 'admin'], function(){
	Route::get('dashboard',[
		'as'	=> 'dashboard',
		'uses'  => 'PostController@index'
	]);
	Route::post('create-post',[
		'as'	=> 'create.posts',
		'uses'  => 'PostController@postCreate'
	]);
	Route::get('delete-post/{id}',[
		'as'	=> 'delete.post',
		'uses'  => 'PostController@destroy'
	]);
	Route::get('edit.post/{id}',[
		'as'	=> 'edit.post/{id}',
		'uses'  => 'PostController@edit'
	]);
	Route::post('update-post',[
		'as'	=> 'update.post',
		'uses'  => 'PostController@update'
	]);
	Route::get('vote-to-post',[
		'as'	=> 'vote-to-post',
		'uses'  => 'PostController@vote'
	]);
	Route::get('user-logout',[
		'as'	=> 'user.logout',
		'uses'  => 'UserController@logout'
	]);


});
Route::post('user-sign-up',[
	'as'	=> 'user-sign-up.post',
	'uses'  => 'UserController@postSignUp'
]);

Route::post('user-login',[
	'as'	=> 'user-login.post',
	'uses'  => 'UserController@postSignIn'
]);


