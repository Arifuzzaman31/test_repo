<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Vote;
use Session;

class PostController extends Controller
{
    
    public function index()
    {
        $posts = Post::orderBy('created_at','DESC')->get();
        // return $posts;
        return view('dashboard',['posts' => $posts]);
    }

    public function postCreate(Request $request)
    {
        // return $request->all();
        $this->validate($request,[
            'body' => 'required|max:1000'
        ]);
        $post = new Post;
        $post->body = $request->body;
        $post->user_id = Session::get('admin_id');
        $post->save();
        return redirect()->route('dashboard');
    }

   
    public function store(Request $request)
    {
        //
    }

    public function vote(Request $request)
    {  
       try {
            $update = Vote::where([
                    'post_id' => $request->post_id,
                    'user_id' => $request->user_id
                ])->first();
            if ($update) {
                $likeun = $update->vote == 0 ? 1 : 0;
                Vote::where([
                    'post_id' => $request->post_id,
                    'user_id' => $request->user_id
                ])->update([
                    'vote' => $likeun
                ]);
                return response()->json(['status' => $likeun, 'message' => 'You the post Change Support!']);
            }else{
                Vote::insert([
                    'post_id' => $request->post_id,
                    'user_id' => $request->user_id,
                    'vote' => 1
                ]);
                return response()->json(['status' => false, 'message' => 'You liked the post!']);
            }    
            
       } catch (Exception $e) {
           return response()->json(['message' => $e->errorInfo[2]]);
       }
        
    }

    public function edit($id)
    {
        // return $id;
        $data = Post::where('id',$id)->first();
        // return $data;
        return view('editpost',['data' => $data]);
    }

    public function update(Request $request)
    {
         $update = Post::where('id',$request->update_id)
                ->update([
                    'body' => $request->edit_body
                ]);
        if ($update) {
            return redirect()->back()->with(['message' => 'Successfully Updated!']);
        }else{
            return redirect()->back()->withErrors(['message' => ' Some problem to Update']);
        }
    }

    public function destroy($id)
    {
        $delete = Post::where('id',$id)->first();
        if (Session::get('admin_id') != $delete->user_id) {
            return redirect()->back()->withErrors(['message' => 'You Cant Delete it!']);
        }else{
            $delete->delete();
            return redirect()->back()->with(['message' => 'Successfully Deleted!']);
        }
    
    }
}
