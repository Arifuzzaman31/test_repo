<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user()
    {
    	return $this->belongsTo('App\User','user_id','id');
    }

    public function voted()
    {
    	return $this->hasOne('App\Vote','post_id','id');
    }
}
